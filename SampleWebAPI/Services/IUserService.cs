﻿using SampleWebAPI.Models;

namespace SampleWebAPI.Services
{
    public interface IUserService
    {
        Task<UserDetails> GetUserDetails(int id);
        Task<CreateUserResponse> CreateUser(CreateUserRequest createUserRequest);

    }
}
